%include "colon.inc"

section .data

colon "pen", key_apple
db "A writing instrument", 0

colon "hand", key_computer
db "A human upper limb from shoulder to toes", 0

colon "knob", key_cat
db "A product for manual door opening", 0
