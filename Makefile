ASM = nasm

result: main.o lib.o dict.o
	ld -o $@ $^

main.o: main.asm dict.inc colon.inc lib.inc words.inc

dict.o: dict.asm lib.inc

%.o: %.asm
	$(ASM) -f elf64 -o $@ $<

clean:
	rm -rf *.o

test: result
	python3 test.py

.PHONY: clean test
