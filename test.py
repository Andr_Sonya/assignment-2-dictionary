import subprocess

test_inputs = ["pen", "hand", "knob", "", "abracadabra", "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"]
expected_outputs = ["A writing instrument", "A human upper limb from shoulder to toes", "A product for manual door opening", "", "", ""]
expected_errors = ["", "", "", "", "", "Element is not found", "Element is not found", "Key is invalid"]
num_failures = 0

print("Starting tests")
print("-------------------")

for i in range(len(test_inputs)):
    process = subprocess.Popen(["./result"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = process.communicate(input=test_inputs[i].encode())
    stdout = stdout.decode().strip()
    stderr = stderr.decode().strip()

    if stdout == expected_outputs[i] and stderr == expected_errors[i]:
        print("Test %s passed" % i)
    else:
        num_failures += 1
        print("Test %s failed" % i)
        print("Input: %s" % test_inputs[i])
        print("Awaited output: %s" % expected_outputs[i])
        print("Stdout: %s" % stdout.strip())
        print("Awaited error: %s" % expected_errors[i])
        print("Stderr: %s" % stderr.strip())
        print("Exit code: %s" % process.returncode)
    print("-------------------")

if num_failures == 0:
    print("All tests passed")
else:
    print("%d tests failed" % num_failures)
