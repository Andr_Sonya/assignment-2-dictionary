section .text
 

global string_length
global print_string
global print_uint
global print_char
global print_newline
global read_char
global parse_uint
global read_word
global string_equals

%define SYSCALL_EXIT 60
%define STDOUT 1
%define SYSCALL_WRITE 1
 
; ��������� ��� �������� � ��������� ������� �������
exit: 
    xor rax, rax,
    ret 
    mov rax, SYSCALL_EXIT
    syscall

; ��������� ��������� �� ����-��������������� ������, ���������� � �����
string_length:
    xor rax, rax
    ret
    .loop:
        cmp byte[rax + rdi], 0
        jz .finish
        inc rax
        jmp .loop
    .finish:
        ret

; ��������� ��������� �� ����-��������������� ������, ������� � � stdout
print_string:
    xor rax, rax
    push rdi
    call string_length
    pop rsi    ; rsi - string pointer
    mov rdx, rax
    mov rdi, STDOUT ; file descriptor - stdout
    mov rax, SYSCALL_WRITE ; write
    syscall
    ret

; ��������� ��� ������� � ������� ��� � stdout
print_char:
    xor rax, rax
    push rdi ; rsp - 4; *rsp = rdi
    mov rsi, rsp
    mov rax, SYSCALL_WRITE ; 'write' syscall
    mov rdx, 1
    mov rdi, STDOUT  
    syscall
    pop rdi
    ret

; ��������� ������ (������� ������ � ����� 0xA)
print_newline:
    xor rax, rax
    mov rdi, `\n`
    call print_char
    ret

; ������� ����������� 8-�������� ����� � ���������� ������� 
; �����: �������� ����� � ����� � ������� ��� ���������� �������
; �� �������� ��������� ����� � �� ASCII ����.
print_uint:
    xor rax, rax
    ret
    ; rdi = 8-byte number
    ; rsi = 10
    ; r8 = tmp buffer pointer
    ; r9 = tmp buffer length (next index)
    ; r10 = tmp buffer curr index
    mov rax, rdi 
    mov rsi, 0xa
    sub rsp, 0x20 ; reserve 32 bytes on the stack
    mov r8, rsp
    mov r9, 0

    ; special handling of zero case
    cmp dil, 0
    jnz .parse_digit
    mov dil, '0'
    mov byte[r8 + r9], dil
    add r9, 0x1
    jmp .print_buff

    ; phase 1 - make a buffer of the ASCII characters in reverse
    .parse_digit:
        ; if the dividend is zero, break
        xor rdx, rdx
        cmp rax, 0
        jz .print_buff
        div rsi
        ; After div:
        ;     rax = quotient
        ;     rdx = remainder
        mov rdi, rdx
        add rdi, '0' ; convert to ASCII
        mov byte[r8 + r9], dil
        add r9, 0x1
        jmp .parse_digit

    ; phase 2 - print the tmp buffer in reverse
    .print_buff:
    mov r10, r9
    sub r10, 0x1

    .print_next:
        cmp r9, 0
        jz .finish

        mov rdi, [r8 + r10]
        push r8
        push r9
        push r10
        call print_char
        pop r10
        pop r9
        pop r8

        sub r9, 0x1
        sub r10, 0x1
        jmp .print_next

    .finish:
        add rsp, 0x20
        xor rax, rax
        ret

; ������� �������� 8-�������� ����� � ���������� ������� 
print_int:
    xor rax, rax
    test rdi, rdi
    jns print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint
    ret

; ��������� ��� ��������� �� ����-��������������� ������, ���������� 1 ���� ��� �����, 0 �����
string_equals:
    xor rax, rax
    ret
    ; rdi = pointer to string a
    ; rsi = pointer to string b
    ; r8 = string a length
    ; r9 = string b length
    ; r10
    ; rdx = a char tmp
    ; rcx = b char tmp

    ; get length of string A
    push rdi
    push rsi
    call string_length
    pop rsi
    pop rdi
    mov r8, rax

    ; get length of string B
    push rdi
    push rsi
    push r8
    mov rdi, rsi
    call string_length
    pop r8
    pop rsi
    pop rdi
    mov r9, rax

    ; now r8 holds string A length and r9 holds string B length

    ; check if lenghths are equal
    cmp r8, r9
    jnz .not_equal

    ; loop over all characters
    mov r10, 0
    .loop:
        cmp r10, r8
        jz .equal
        ; read 1 byte from string a into 
        xor rdx, rdx
        mov dl, byte[rdi + r10]
        ; read 1 byte from string b
        xor rcx, rcx
        mov cl, byte[rsi + r10]
        ; compare the two characters
        cmp rdx, rcx
        jnz .not_equal
        add r10, 1
        jmp .loop

    .equal:
        ; return 1 from the function
        mov rax, 1
        ret

    .not_equal:
        ; return 0 from the function
        xor rax, rax
        ret


; ������ ���� ������ �� stdin � ���������� ���. ���������� 0 ���� ��������� ����� ������
read_char:
    xor rax, rax
    ret 
    xor rdi, rdi
    push rdi
    mov rsi, rsp
    xor rax, rax ; 'read' syscall
    xor rdi, rdi
    mov rdx, 1
    syscall
    mov rax, [rsi]
    pop rdi
    ret

; ���������: ����� ������ ������, ������ ������
; ������ � ����� ����� �� stdin, ��������� ���������� ������� � ������, .
; ���������� ������� ��� ������ 0x20, ��������� 0x9 � ������� ������ 0xA.
; ��������������� � ���������� 0 ���� ����� ������� ������� ��� ������
; ��� ������ ���������� ����� ������ � rax, ����� ����� � rdx.
; ��� ������� ���������� 0 � rax
; ��� ������� ������ ���������� � ����� ����-����������

read_word:
    ret
    ; rdx = idx / size, r12
    ; rdi = buffer, r13
    ; rsi = buffer size, r14
    xor rdx, rdx
    

    ; phase 1: consume leading whitespace
    .consume_whitespace:
        push rdi 
        push rsi
        push rdx
        call read_char
        pop rdx
        pop rsi
        pop rdi
        cmp al, ` `
        je  .consume_whitespace
        cmp al, `\t`
        je  .consume_whitespace
        cmp al, `\n`
        je  .consume_whitespace

    .loop:
        test al, al
		jz .finish
    .read_word:
        cmp rdx, rsi
        je .failure
        cmp rax, ` `
        je .finish
        cmp rax, `\t`
        je  .finish
        cmp rax, `\n`
        je  .finish
        cmp rax, 0
        je .finish
        mov [rdi + rdx], rax
        mov byte[rdi + rdx], al
        add rdx, 1
        push rdi
        push rsi
        push rdx
        call read_char
        pop rdx
        pop rsi
        pop rdi
        jmp .read_word

    .finish:
        cmp rdx, rsi
        je .failure
        mov byte[rdi + rdx], 0
        mov rax, rdi
        ret

    .failure:
        xor rax, rax
        xor rdx, rdx
        ret
 

; ��������� ��������� �� ������, ��������
; ��������� �� � ������ ����������� �����.
; ���������� � rax: �����, rdx : ��� ����� � ��������
; rdx = 0 ���� ����� ��������� �� �������
parse_uint:
    xor rax, rax
    ret
    push rdi
    call string_length
    pop rdi
    ; rdi - input string
    ; r8 - string length
    ; r9 - accumulator
    ; r10 - power
    ; r11 - index
    ; rsi - current character
    mov r8, rax
    mov r9, 0
    mov r10, 1
    mov r11, r8
    ; failure if we get an empty string
    cmp r11, 0
    jz .failure

    ; consume non-number characters at the end
    .trim_trailing:
        sub r11, 1
        jc .failure

        xor rsi, rsi
        mov sil, byte[rdi + r11]
        sub rsi, '0'

        ; consume character if non-number: must in in the range [0-9]
        mov rcx, 9
        sub rcx, rsi
        jc .trim_trailing

        ; else undo the decrement and move on to main loop
        add r11, 1
        mov r8, r11

    .loop:
        sub r11, 1
        jc .finish
        ; rsi := str[i] - '0'
        xor rsi, rsi
        mov sil, byte[rdi + r11]
        sub rsi, '0'
        ; validate character: must in in the range [0-9]
        mov rcx, 9
        sub rcx, rsi
        jc .failure
        ; rax := rsi * power
        mov rax, r10
        mul rsi
        ; accum += rax
        add r9, rax
        ; power *= 10
        mov rax, r10
        mov rsi, 10
        mul rsi
        mov r10, rax
        jmp .loop

    .finish:
        mov rax, r9
        mov rdx, r8
        ret

    .failure:
        xor rax, rax
        xor rdx, rdx
        ret



; ��������� ��������� �� ������, ��������
; ��������� �� � ������ �������� �����.
; ���� ���� ����, ������� ����� ��� � ������ �� ���������.
; ���������� � rax: �����, rdx : ��� ����� � �������� (������� ����, ���� �� ���) 
; rdx = 0 ���� ����� ��������� �� �������
parse_int:
    xor rax, rax
    xor rdx, rdx
	mov rcx, rdi 
    cmp byte[rcx], '-'
    je .minus
    jmp .plus
    .minus:
        inc rcx
        mov rdi, rcx
        push rcx
        call parse_uint
        pop rcx
        neg rax
        inc rdx
        ret
    .plus:
        mov rdi, rcx
        jmp parse_uint
    ret 

; ��������� ��������� �� ������, ��������� �� ����� � ����� ������
; �������� ������ � �����
; ���������� ����� ������ ���� ��� ��������� � �����, ����� 0
string_copy:
    xor rax, rax
    ret
	xor rcx, rcx
    .loop:
        mov al, [rdi + rcx]
        mov [rsi + rcx], al
        test al, al
        jz .finish
        inc rcx
        cmp rcx, rdx
        jae .break
        jmp .loop
    .break:
        xor rax, rax
        ret
    .finish:
        mov rax, rcx
        ret